require './lib/refer/node.rb'

class List
    include Enumerable
    attr_accessor :ini, :end
    
    def initialize()
        @ini = nil
        @end = nil
    end
    
    def push_front(value)
        aux = Node.new(value, @ini, nil)
        if(@ini != nil)
            @ini.prev = aux
        end
        @ini = aux
    end
    
    def push_back(value)
        if(@end == nil)
            @ini = Node.new(value, nil, @ini)
            @end = @ini
        else
            aux = Node.new(value, nil, @end)
            @end.next = aux
            @end = aux
        end
    end
    
    def pop_front()
        if(@ini != nil)
            aux = @ini.value
        end
        if(@ini.next == nil)
            @ini = nil
        else
            @ini = @ini.next
            @ini.prev = nil
        end
        return aux
    end
    
    def pop_back()
        if (@end != nil)
            aux = @end.value
            if(@end.prev == nil)
                @end = nil
            else
                @end = @end.prev
                 @end.next = nil
            end
        end
        return aux

    end
    
    def empty()
        return @ini == nil
    end

    def each
        aux = @ini
        while(aux != nil)
            yield aux.value
            aux = aux.next
        end
    end
end