class Biblio
    attr_accessor :autores, :titulo, :serie, :editorial, :edicion, :fecha
    
    def initialize(autores, titulo, serie=nil, editorial, edicion, fecha)
        @autores = autores
        @titulo = titulo
        @serie = serie;
        @editorial = editorial
        @edicion = edicion
        @fecha = fecha
    end

    def to_s
        "#{@autores}\n#{@titulo}\n#{@serie}\n#{@editorial}\n#{@edicion}\n#{@fecha}"
    end
end

class Book < Biblio
    attr_accessor :isbn
    def initialize(autores, titulo, serie=nil, editorial, edicion, fecha, isbn)
        super(autores, titulo, serie=nil, editorial, edicion, fecha)
        @isbn = isbn
    end
end

class Magazine < Biblio
    attr_accessor :numero
    def initialize(autores, titulo, serie=nil, editorial, edicion, fecha, numero)
        super(autores, titulo, serie=nil, editorial, edicion, fecha)
        @numero = numero
    end
end

class EBook < Biblio
    attr_accessor :url
    def initialize(autores, titulo, serie=nil, editorial, edicion, fecha, url)
        super(autores, titulo, serie=nil, editorial, edicion, fecha)
        @url = url
    end
end

var = Book.new(["Dave Thomas","Andy Hunt","Chad Flower"],"Programming Ruby","The Facets of Ruby","Pragmatic Bookshelf",4,"07-jul-13", "0100601398")