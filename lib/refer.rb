#require "refer/biblio"
#require "refer/version"

module Refer
    class Biblio
        attr_accessor :autores, :titulo, :serie, :editorial, :edicion, :fecha, :isbn
        
        def initialize(autores, titulo, serie=nil, editorial, edicion, fecha, isbn)
            @autores = autores
            @titulo = titulo
            @serie = serie;
            @editorial = editorial
            @edicion = edicion
            @fecha = fecha
            @isbn = isbn
        end
    
        def to_s
            "#{@autores}\n#{@titulo}\n#{@serie}\n#{@editorial}\n#{@edicion}\n#{@fecha}\nISBN: #{@isbn}"
        end
    end
    
    class Book < Biblio
        attr_accessor :isbn
        def initialize(autores, titulo, serie=nil, editorial, edicion, fecha, isbn)
            super(autores, titulo, serie=nil, editorial, edicion, fecha)
            @isbn = isbn
        end
    end
    
    class Magazine < Biblio
        attr_accessor :numero
        def initialize(autores, titulo, serie=nil, editorial, edicion, fecha, numero)
            super(autores, titulo, serie=nil, editorial, edicion, fecha)
            @numero = numero
        end
    end
    
    class EBook < Biblio
        attr_accessor :url
        def initialize(autores, titulo, serie=nil, editorial, edicion, fecha, url)
            super(autores, titulo, serie=nil, editorial, edicion, fecha)
            @url = url
        end
    end
end
